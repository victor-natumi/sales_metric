package br.com.carrefour.business

// built-in
import org.slf4j.LoggerFactory
// thrid party
import org.apache.spark.sql.expressions.UserDefinedAggregateFunction
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._
import org.apache.spark.sql.{Column, DataFrame, Row}
// local
import br.com.carrefour.model._
import br.com.carrefour.utils._

class SalesMetricBusiness(date: String, view: String) {
  val log = LoggerFactory.getLogger(this.getClass)
  
  val dropUseless = udf((xs: Seq[Row]) =>  xs.map{row =>
        Row(
          row.getAs(UdafModel.COD_PRODUCT_RMS)
          , row.getAs(UdafModel.QTY_ITEM)
          , row.getAs(UdafModel.VAL_UNIT_PRICE)
          , row.getAs(UdafModel.VAL_TOTAL_WITH_DISCOUNT)
          , row.getAs(UdafModel.COD_BUS)
          , row.getAs(UdafModel.DS_SECTOR)
          , row.getAs(UdafModel.DS_DEPARTMENT)
          , row.getAs(UdafModel.DS_GROUP)
        )
    }, ArrayType(UdafModel.ItemClientSchema))

  def transformDF(dataFrame: DataFrame): DataFrame = {  
    dataFrame.withColumn(
      SalesModel.Field.LST_SALES_ITEMS
      , dropUseless(col(SalesModel.Field.LST_SALES_ITEMS))
    )
  }
  
  def transform_for_client_registered(dataFrame: DataFrame): DataFrame = {
    dataFrame.
    where(col(SalesModel.Field.IND_REGISTERED) === "1").
    withColumn(
      SalesModel.Field.LST_SALES_ITEMS
      , dropUseless(col(SalesModel.Field.LST_SALES_ITEMS))
    )
  }
  
  def transform_for_format(dataFrame: DataFrame): DataFrame = {
    val getkpiGroup = udf((xs: String) => { 
        val allKpi: Seq[String] = Seq("hipermercados","supermercados","express","drogaria","postos","e-commerce")
        try {
          val index = allKpi.indexOf("supermercados")
          val format = allKpi(index).toUpperCase
          val newAllKpi = allKpi.patch(index, Seq(format), 1)
          val combinationsList = newAllKpi.toSet.subsets.filter(_.contains(format)).map(m => m.toSeq.sortWith(_.toUpperCase < _.toUpperCase).mkString("|")).toList
          combinationsList
        } catch {
          case e: Exception => {
            List(xs)
          }
        }
      }
    )
    dataFrame.withColumn(
      SalesModel.Field.LST_SALES_ITEMS
      , dropUseless(col(SalesModel.Field.LST_SALES_ITEMS))
    ).withColumn(
      SalesModel.Field.DS_SITE_FORMAT
      , getkpiGroup(col(SalesModel.Field.DS_SITE_FORMAT))
    ).select(
      col(SalesModel.Field.NUM_CONSUMER_DOCUMENT_PRIORITY)
      , col(SalesModel.Field.COD_SITE)
      , col(SalesModel.Field.COD_UF)
      , col(SalesModel.Field.ID_SOURCE_SALE)
      , explode(col(SalesModel.Field.DS_SITE_FORMAT)).alias(SalesModel.Field.DS_SITE_FORMAT)
      , col(SalesModel.Field.COD_IND_SITE_ECOMM)
      , col(SalesModel.Field.LST_SALES_ITEMS)
      , col(SalesModel.Field.COD_DELIVERY)
      , col(SalesModel.Field.VAL_DOCUMENT_TOTAL_WITH_SERVICES)
      , col(SalesModel.Field.QTY_TOTAL_COUPON_BURNT)
    )
  }

  def transform_for_sector(dataframe:DataFrame): DataFrame = {
    dataframe.withColumn(
      SalesModel.Field.LST_SALES_ITEMS
      , explode(dropUseless(col(SalesModel.Field.LST_SALES_ITEMS)))
    ).withColumn(
      SalesModel.ItemField.DS_SECTOR
      , col(s"${SalesModel.Field.LST_SALES_ITEMS}.${SalesModel.ItemField.DS_SECTOR}")
    ).groupBy(
      col(SalesModel.ItemField.DS_SECTOR)
      , col(SalesModel.Field.ID_SOURCE_SALE)
      , col(SalesModel.Field.COD_UF)
      , col(SalesModel.Field.DS_SITE_FORMAT)
      , col(SalesModel.Field.COD_IND_SITE_ECOMM)
      , col(SalesModel.Field.COD_DELIVERY)
      , col(SalesModel.Field.VAL_DOCUMENT_TOTAL_WITH_SERVICES)
      , col(SalesModel.Field.QTY_TOTAL_COUPON_BURNT)
    ).agg(collect_list(col(SalesModel.Field.LST_SALES_ITEMS)).alias(SalesModel.Field.LST_SALES_ITEMS))
  }
 
  def getResult(df: DataFrame): DataFrame = {
    log.info(s"[*] Id View: ${view}")
    log.info(s"[*] View: ${getMetricViewField}")

    val getkpi = udf((xs: String) => MetricsModel.getKpi(xs))
  
    if(view == "8"){
      df.select(
        col(MetricsModel.Field.ID_VIEW_DOCUMENT).cast(LongType)
        , col(MetricsModel.Field.DS_VIEW_DOCUMENT).cast(StringType)
        , col(SalesModel.Field.COD_UF).cast(LongType)
        , col(MetricsModel.Field.ID_METRIC).cast(StringType)
        , col(MetricsModel.Field.DS_METRIC).cast(StringType)
        , posexplode(col("metric"))
      ).
      withColumn(MetricsModel.Field.DS_KPI, getkpi(col("pos"))).
      withColumn(MetricsModel.Field.DTH_INGESTION, current_timestamp()).
      withColumn(MetricsModel.Field.COD_YEAR, lit(DateUtil.getYear(date))).
      withColumn(MetricsModel.Field.COD_MONTH, lit(DateUtil.getMonth(date))).
      withColumn(MetricsModel.Field.NUM_DOCUMENT, col(SalesModel.Field.COD_UF)).
      select(
         col(MetricsModel.Field.ID_VIEW_DOCUMENT).cast(IntegerType)
        , col(MetricsModel.Field.DS_VIEW_DOCUMENT).cast(StringType)
        , col(MetricsModel.Field.NUM_DOCUMENT).cast(StringType)
        , col(SalesModel.Field.COD_UF).cast(StringType) as MetricsModel.Field.COD_UF
        , col(MetricsModel.Field.DS_METRIC).cast(StringType)
        , col("pos").cast(IntegerType) as MetricsModel.Field.ID_KPI
        , col(MetricsModel.Field.DS_KPI).cast(StringType)
        , col("col").cast(DecimalType(38,10)) as MetricsModel.Field.VAL_KPI
        , col(MetricsModel.Field.DTH_INGESTION).cast(TimestampType)
        , col(MetricsModel.Field.COD_YEAR).cast(IntegerType)
        , col(MetricsModel.Field.COD_MONTH).cast(IntegerType)
        , col(MetricsModel.Field.ID_METRIC).cast(IntegerType)
      )
    } else {
      df.select(
        col(MetricsModel.Field.ID_VIEW_DOCUMENT).cast(LongType)
        , col(MetricsModel.Field.DS_VIEW_DOCUMENT).cast(StringType)
        , col(getMetricViewField)
        , col(SalesModel.Field.COD_UF).cast(LongType)
        , col(MetricsModel.Field.ID_METRIC).cast(StringType)
        , col(MetricsModel.Field.DS_METRIC).cast(StringType)
        , posexplode(col("metric"))
      ).
      withColumn(MetricsModel.Field.DS_KPI, getkpi(col("pos"))).
      withColumn(MetricsModel.Field.DTH_INGESTION, current_timestamp()).
      withColumn(MetricsModel.Field.COD_YEAR, lit(DateUtil.getYear(date))).
      withColumn(MetricsModel.Field.COD_MONTH, lit(DateUtil.getMonth(date))).
      select(
         col(MetricsModel.Field.ID_VIEW_DOCUMENT).cast(IntegerType)
        , col(MetricsModel.Field.DS_VIEW_DOCUMENT).cast(StringType)
        , col(getMetricViewField).cast(StringType) as MetricsModel.Field.NUM_DOCUMENT
        , col(SalesModel.Field.COD_UF).cast(StringType) as MetricsModel.Field.COD_UF
        , col(MetricsModel.Field.DS_METRIC).cast(StringType)
        , col("pos").cast(IntegerType) as MetricsModel.Field.ID_KPI
        , col(MetricsModel.Field.DS_KPI).cast(StringType)
        , col("col").cast(DecimalType(38,10)) as MetricsModel.Field.VAL_KPI
        , col(MetricsModel.Field.DTH_INGESTION).cast(TimestampType)
        , col(MetricsModel.Field.COD_YEAR).cast(IntegerType)
        , col(MetricsModel.Field.COD_MONTH).cast(IntegerType)
        , col(MetricsModel.Field.ID_METRIC).cast(IntegerType)
      )
    }
  }

  def getMetricUDAF(classUDAF: String): UserDefinedAggregateFunction = {
    Class.forName(s"br.com.carrefour.business.${classUDAF}").newInstance().asInstanceOf[UserDefinedAggregateFunction]
  }
/*
  def renameColumnsDF(df_input:DataFrame): DataFrame = {
    getMetricViewFields.foldLeft(df_input)((acc, m) => acc.withColumnRenamed(s"${m}_m", s"${m}"))
  }  
*/
  def applyMetric(df: DataFrame, metric:String): DataFrame = {
    val metricDF = 
      df.groupBy(getMetricViewFields.map(m => upper(m).alias(s"${m.toString}")):_*).
      agg(getMetricUDAF(MetricsModel.code_metrics(metric))(UdafModel.ClientSchema.fieldNames.map(i => col(i)).toSeq: _*) as "metric").
      withColumn(MetricsModel.Field.ID_METRIC, lit(metric)).
      withColumn(MetricsModel.Field.DS_METRIC, lit(MetricsModel.getMetric(metric))).
      withColumn(MetricsModel.Field.ID_VIEW_DOCUMENT, lit(if (view == null) 1 else view.toInt)).
      withColumn(MetricsModel.Field.DS_VIEW_DOCUMENT, lit(MetricsModel.getDsView(if (view == null) "1" else view )))
    //renameColumnsDF(metricDF)
    metricDF
  }
  
  def getMetricViewField(): String = view match {
    case "1" | "11" => SalesModel.Field.NUM_CONSUMER_DOCUMENT_PRIORITY
    case "2" => SalesModel.Field.COD_SITE
    case "3" => SalesModel.ItemField.DS_SECTOR
    case "4" => ""
    case "5" => ""
    case "6" => SalesModel.Field.DS_SITE_FORMAT
    case "7" => ""
    case "8" => SalesModel.Field.COD_UF
    case _ => SalesModel.Field.NUM_CONSUMER_DOCUMENT_PRIORITY
  }

  def getMetricViewFields():Seq[Column] = view match {
    case "1" | "11" => Seq(col(SalesModel.Field.NUM_CONSUMER_DOCUMENT_PRIORITY), col(SalesModel.Field.COD_UF))
    case "2" => Seq(col(SalesModel.Field.COD_SITE), col(SalesModel.Field.COD_UF))
    case "3" => Seq(col(SalesModel.ItemField.DS_SECTOR), col(SalesModel.Field.COD_UF))
    case "4" => Seq(col(SalesModel.Field.COD_UF))
    case "5" => Seq(col(SalesModel.Field.COD_UF))
    case "6" => Seq(col(SalesModel.Field.DS_SITE_FORMAT), col(SalesModel.Field.COD_UF)) 
    case "7" => Seq(col(SalesModel.Field.COD_UF)) 
    case "8" => Seq(col(SalesModel.Field.COD_UF)) 
    case _ => Seq(col(SalesModel.Field.NUM_CONSUMER_DOCUMENT_PRIORITY), col(SalesModel.Field.COD_UF))
  }

}

object SalesMetricBusiness {
  val log = LoggerFactory.getLogger(this.getClass)
  var instance: SalesMetricBusiness = null

  def getInstance(date: String, view: String) = {
    log.info("[*] Getting instance")
    if (instance == null)
      instance = new SalesMetricBusiness(date, view)
    instance
  }
}
