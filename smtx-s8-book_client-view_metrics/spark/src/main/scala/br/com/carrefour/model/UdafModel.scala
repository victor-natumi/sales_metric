package br.com.carrefour.model

import org.apache.spark.sql.types._

object UdafModel {

  val ID_SOURCE_SALE = SalesModel.Field.ID_SOURCE_SALE
  val DS_SITE_FORMAT = SalesModel.Field.DS_SITE_FORMAT
  val COD_IND_SITE_ECOMM = SalesModel.Field.COD_IND_SITE_ECOMM
  val LST_SALES_ITEMS = SalesModel.Field.LST_SALES_ITEMS
  val COD_DELIVERY = SalesModel.Field.COD_DELIVERY
  val VAL_DOCUMENT_TOTAL_WITH_SERVICES = SalesModel.Field.VAL_DOCUMENT_TOTAL_WITH_SERVICES
  val QTY_TOTAL_COUPON_BURNT = SalesModel.Field.QTY_TOTAL_COUPON_BURNT
  val LST_COUPON_BURNT = SalesModel.Field.LST_COUPON_BURNT

  val COD_PRODUCT_RMS = SalesModel.ItemField.COD_PRODUCT_RMS
  val QTY_ITEM = SalesModel.ItemField.QTY_ITEM
  val VAL_UNIT_PRICE = SalesModel.ItemField.VAL_UNIT_PRICE
  val VAL_TOTAL_WITH_DISCOUNT = SalesModel.ItemField.VAL_TOTAL_WITH_DISCOUNT
  val COD_BUS = SalesModel.ItemField.COD_BUS 
  val DS_SECTOR = SalesModel.ItemField.DS_SECTOR
  val DS_DEPARTMENT = SalesModel.ItemField.DS_DEPARTMENT
  val DS_GROUP = SalesModel.ItemField.DS_GROUP

  val ItemClientSchema: StructType = {
    StructType(
      StructField(SalesModel.ItemField.COD_PRODUCT_RMS, DecimalType(38,10))
      :: StructField(SalesModel.ItemField.QTY_ITEM, DecimalType(38,10))
      :: StructField(SalesModel.ItemField.VAL_UNIT_PRICE, DecimalType(38,10))
      :: StructField(SalesModel.ItemField.VAL_TOTAL_WITH_DISCOUNT, DecimalType(38,10))
      :: StructField(SalesModel.ItemField.COD_BUS, StringType)
      :: StructField(SalesModel.ItemField.DS_SECTOR, StringType)
      :: StructField(SalesModel.ItemField.DS_DEPARTMENT, StringType)
      :: StructField(SalesModel.ItemField.DS_GROUP, StringType)
      :: Nil
    )
  }
  
  val ClientSchema: StructType = {
    StructType(
        StructField(SalesModel.Field.ID_SOURCE_SALE, IntegerType)
        :: StructField(SalesModel.Field.DS_SITE_FORMAT, StringType)
        :: StructField(SalesModel.Field.COD_IND_SITE_ECOMM, IntegerType)
        :: StructField(SalesModel.Field.LST_SALES_ITEMS, ArrayType(ItemClientSchema))
        :: StructField(SalesModel.Field.COD_DELIVERY, IntegerType)
        :: StructField(SalesModel.Field.VAL_DOCUMENT_TOTAL_WITH_SERVICES, DecimalType(38,10))
        :: StructField(SalesModel.Field.QTY_TOTAL_COUPON_BURNT, DecimalType(38,10))
        :: Nil
    )
  }

  val ResultSchema = ArrayType(DoubleType)
 
}
