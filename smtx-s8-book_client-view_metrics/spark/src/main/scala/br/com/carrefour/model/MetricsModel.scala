package br.com.carrefour.model

import br.com.carrefour.utils.ConfigUtil
import org.apache.spark.sql.types._

object MetricsModel {
  val DB = "db_dolphin_target"
  val TABLE = "tbl_metrics"
  val DB_TABLE = s"${DB}.${TABLE}"

  val code_metrics = ConfigUtil.getMap("metric.code_metric")
  val getMetric = ConfigUtil.getMap("metric.desc_metric") 
  val desc_metric = ConfigUtil.getMap("metric.desc_metric") 
  val getKpi = ConfigUtil.getMap("metric.desc_kpi")
  val getDsView = ConfigUtil.getMap("metric.desc_view")

  object Field {
    val NUM_DOCUMENT = "id_view"
    val COD_UF = "cod_uf"
    val COD_YEAR = "cod_year"
    val COD_MONTH = "cod_month"
    val ID_METRIC = "id_grp_metric"
    val ID_KPI = "id_metric"
    val VAL_KPI = "val_metric"
    val DTH_INGESTION = "dth_ingestion"
    val DS_KPI = "ds_metric"
    val DS_METRIC = "ds_grp_metric"

    val ID_VIEW_DOCUMENT = "id_view_metric"
    val DS_VIEW_DOCUMENT = "ds_view_metric"

    val Schema: StructType = {
      StructType(
        StructField(DS_VIEW_DOCUMENT, StringType)
          :: StructField(NUM_DOCUMENT, StringType)
          :: StructField(COD_UF, StringType)
          :: StructField(DS_METRIC, StringType)
          :: StructField(ID_KPI, IntegerType)
          :: StructField(DS_KPI, StringType)
          :: StructField(VAL_KPI, DecimalType(38, 10))
          :: StructField(DTH_INGESTION, TimestampType)
          :: StructField(COD_YEAR, IntegerType)
          :: StructField(COD_MONTH, IntegerType)
          :: StructField(ID_VIEW_DOCUMENT, IntegerType)
          :: StructField(ID_METRIC, IntegerType)
          :: Nil
      )
    }
  }

  val rows: Seq[String] = Seq(
    Field.DS_VIEW_DOCUMENT
    , Field.NUM_DOCUMENT   
    , Field.COD_UF
    , Field.DS_METRIC
    , Field.ID_KPI
    , Field.DS_KPI
    , Field.VAL_KPI
    , Field.DTH_INGESTION
    , Field.ID_METRIC
  )
}
