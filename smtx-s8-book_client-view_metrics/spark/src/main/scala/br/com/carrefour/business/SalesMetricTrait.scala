package br.com.carrefour.business

import br.com.carrefour.model._

trait SalesMetricTrait {
 
  val idx_id_source_sale = UdafModel.ClientSchema.fieldIndex(UdafModel.ID_SOURCE_SALE)
  val idx_ds_site_format = UdafModel.ClientSchema.fieldIndex(UdafModel.DS_SITE_FORMAT)
  val idx_cod_ind_site_ecomm = UdafModel.ClientSchema.fieldIndex(UdafModel.COD_IND_SITE_ECOMM)
  val idx_lst_sales_items = UdafModel.ClientSchema.fieldIndex(UdafModel.LST_SALES_ITEMS)
  val idx_cod_delivery = UdafModel.ClientSchema.fieldIndex(UdafModel.COD_DELIVERY)
  val idx_cod_bus = UdafModel.ItemClientSchema.fieldIndex(UdafModel.COD_BUS)
  val idx_cod_product_rms = UdafModel.ItemClientSchema.fieldIndex(UdafModel.COD_PRODUCT_RMS)
  val idx_val_document_total_with_services = UdafModel.ClientSchema.fieldIndex(UdafModel.VAL_DOCUMENT_TOTAL_WITH_SERVICES)
  val idx_val_unit_price = UdafModel.ItemClientSchema.fieldIndex(UdafModel.VAL_UNIT_PRICE)
  val idx_val_total_with_discount = UdafModel.ItemClientSchema.fieldIndex(UdafModel.VAL_TOTAL_WITH_DISCOUNT)
  val idx_ds_sector = UdafModel.ItemClientSchema.fieldIndex(UdafModel.DS_SECTOR)
  val idx_ds_department = UdafModel.ItemClientSchema.fieldIndex(UdafModel.DS_DEPARTMENT)
  val idx_ds_group = UdafModel.ItemClientSchema.fieldIndex(UdafModel.DS_GROUP)
  val idx_qty_total_coupon_burnt = UdafModel.ClientSchema.fieldIndex(UdafModel.QTY_TOTAL_COUPON_BURNT)
  val idx_qty_item = UdafModel.ItemClientSchema.fieldIndex(UdafModel.QTY_ITEM)

}
