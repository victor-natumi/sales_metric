package br.com.carrefour.business

import br.com.carrefour.model.UdafModel
import org.apache.spark.sql.Row
import org.apache.spark.sql.expressions.{MutableAggregationBuffer, UserDefinedAggregateFunction}
import org.apache.spark.sql.types._

class QtySectorsUDAF extends UserDefinedAggregateFunction with SalesMetricTrait {

  // Input Data Type Schema
  override def inputSchema: StructType = UdafModel.ClientSchema

  // Intermediate Schema
  override def bufferSchema: StructType =
    StructType(
      StructField("total", ArrayType(StringType))
      :: StructField("store", ArrayType(StringType))
      :: StructField("hyp", ArrayType(StringType))
      :: StructField("sup", ArrayType(StringType))
      :: StructField("prx", ArrayType(StringType))
      :: StructField("drg", ArrayType(StringType))
      :: StructField("gas", ArrayType(StringType))
      :: StructField("ecm", ArrayType(StringType))
      :: StructField("std", ArrayType(StringType))
      :: StructField("click", ArrayType(StringType))
      :: StructField("drive", ArrayType(StringType))
      :: StructField("scan", ArrayType(StringType))
      :: StructField("rappi", ArrayType(StringType))
      :: StructField("other", ArrayType(StringType))
      :: StructField("food", ArrayType(StringType))
      :: StructField("nfood", ArrayType(StringType))
      :: Nil
    )

  // Returned Data Type
  override def dataType: DataType = UdafModel.ResultSchema

  // Self-explaining
  override def deterministic: Boolean = true

  // This function is called whenever key changes
  override def initialize(buffer: MutableAggregationBuffer): Unit = {
    for (i <- 0 until buffer.size) buffer(i) = Seq()
  }

  // Iterate over each entry of a group
  override def update(buffer: MutableAggregationBuffer, input: Row): Unit = {
    if(buffer != null) {
      val id_source_sale: Int = input.get(idx_id_source_sale).asInstanceOf[Int]
      val ds_site_format: String = input.get(idx_ds_site_format).asInstanceOf[String]
      val cod_ind_site_ecomm: Int = input.get(idx_cod_ind_site_ecomm).asInstanceOf[Int]
      val cod_delivery: Int = input.get(idx_cod_delivery).asInstanceOf[Int]
      val lst_sales_items: Seq[Row] = input.get(idx_lst_sales_items).asInstanceOf[Seq[Row]]
      val cod_bus: Seq[String] = lst_sales_items.filter(i => !i.isNullAt(idx_cod_bus)).map(i => i.get(idx_cod_bus).toString).distinct

      val itemsF =
        lst_sales_items.
          filter(i => !i.isNullAt(idx_cod_bus) && !i.isNullAt(idx_ds_department)).
          map(i => (
            i.get(idx_cod_bus).toString
            , Seq(i.get(idx_ds_department))
          ))

      val lst_cod_bus: Map[String, Seq[String]] = itemsF.groupBy(_._1).mapValues(s => s.map(_._2).reduce(_ ++ _).distinct.asInstanceOf[Seq[String]])
      val sectorAmount: Seq[String] = lst_cod_bus.values.toSeq(0)
      
      val metric = new SalesMetricClassifier().check(id_source_sale, ds_site_format, cod_ind_site_ecomm, cod_delivery, cod_bus)
        //---------Counts------------------
        buffer(0) =  if (metric("total"))  { buffer(0).asInstanceOf[Seq[String]] ++ sectorAmount } else { buffer(0).asInstanceOf[Seq[String]] }
        buffer(1) =  if (metric("store"))  { buffer(1).asInstanceOf[Seq[String]] ++ sectorAmount } else { buffer(1).asInstanceOf[Seq[String]] }
        buffer(2) =  if (metric("hyp"))    { buffer(2).asInstanceOf[Seq[String]] ++ sectorAmount } else { buffer(2).asInstanceOf[Seq[String]] }
        buffer(3) =  if (metric("sup"))    { buffer(3).asInstanceOf[Seq[String]] ++ sectorAmount } else { buffer(3).asInstanceOf[Seq[String]] }
        buffer(4) =  if (metric("prx"))    { buffer(4).asInstanceOf[Seq[String]] ++ sectorAmount } else { buffer(4).asInstanceOf[Seq[String]] }
        buffer(5) =  if (metric("drg"))    { buffer(5).asInstanceOf[Seq[String]] ++ sectorAmount } else { buffer(5).asInstanceOf[Seq[String]] }
        buffer(6) =  if (metric("gas"))    { buffer(6).asInstanceOf[Seq[String]] ++ sectorAmount } else { buffer(6).asInstanceOf[Seq[String]] }
        buffer(7) =  if (metric("ecm"))    { buffer(7).asInstanceOf[Seq[String]] ++ sectorAmount } else { buffer(7).asInstanceOf[Seq[String]] }
        buffer(8) =  if (metric("std"))    { buffer(8).asInstanceOf[Seq[String]] ++ sectorAmount } else { buffer(8).asInstanceOf[Seq[String]] }
        buffer(9) =  if (metric("click"))  { buffer(9).asInstanceOf[Seq[String]] ++ sectorAmount } else { buffer(9).asInstanceOf[Seq[String]] }
        buffer(10) =  if (metric("drive"))  { buffer(10).asInstanceOf[Seq[String]] ++ sectorAmount } else { buffer(10).asInstanceOf[Seq[String]] }
        buffer(11) =  if (metric("scan"))   { buffer(11).asInstanceOf[Seq[String]] ++ sectorAmount } else { buffer(11).asInstanceOf[Seq[String]] }
        buffer(12) =  if (metric("rappi"))  { buffer(12).asInstanceOf[Seq[String]] ++ sectorAmount } else { buffer(12).asInstanceOf[Seq[String]] }
        buffer(13) =  if (metric("other"))  { buffer(13).asInstanceOf[Seq[String]] ++ sectorAmount } else { buffer(13).asInstanceOf[Seq[String]] }
        buffer(14) =  if (metric("food"))   { buffer(14).asInstanceOf[Seq[String]] ++ lst_cod_bus.getOrElse("FOOD", Seq()) } else { buffer(14).asInstanceOf[Seq[String]] }
        buffer(15) =  if (metric("nfood"))  { buffer(15).asInstanceOf[Seq[String]] ++ lst_cod_bus.getOrElse("NFOOD", Seq()) } else { buffer(15).asInstanceOf[Seq[String]] }
    }
  }

  def merge(buffer1: MutableAggregationBuffer, buffer2: Row): Unit = {
    if (buffer1 != null && buffer2 != null) {
      for (i <- 0 until buffer1.size) buffer1(i) = (buffer1(i).asInstanceOf[Seq[String]] ++ buffer2(i).asInstanceOf[Seq[String]]).distinct
    }
  }

  // Called after all the entries are exhausted.
  override def evaluate(buffer: Row): Any = {
    Seq(
        if (buffer(0).asInstanceOf[Seq[String]].isEmpty)   {0.0} else { buffer(0).asInstanceOf[Seq[String]].distinct.size }
        , if (buffer(1).asInstanceOf[Seq[String]].isEmpty) {0.0} else { buffer(1).asInstanceOf[Seq[String]].distinct.size }
        , if (buffer(2).asInstanceOf[Seq[String]].isEmpty) {0.0} else { buffer(2).asInstanceOf[Seq[String]].distinct.size }
        , if (buffer(3).asInstanceOf[Seq[String]].isEmpty) {0.0} else { buffer(3).asInstanceOf[Seq[String]].distinct.size }
        , if (buffer(4).asInstanceOf[Seq[String]].isEmpty) {0.0} else { buffer(4).asInstanceOf[Seq[String]].distinct.size }
        , if (buffer(5).asInstanceOf[Seq[String]].isEmpty) {0.0} else { buffer(5).asInstanceOf[Seq[String]].distinct.size }
        , if (buffer(6).asInstanceOf[Seq[String]].isEmpty) {0.0} else { buffer(6).asInstanceOf[Seq[String]].distinct.size }
        , if (buffer(7).asInstanceOf[Seq[String]].isEmpty) {0.0} else { buffer(7).asInstanceOf[Seq[String]].distinct.size }
        , if (buffer(8).asInstanceOf[Seq[String]].isEmpty) {0.0} else { buffer(8).asInstanceOf[Seq[String]].distinct.size }
        , if (buffer(9).asInstanceOf[Seq[String]].isEmpty) {0.0} else { buffer(9).asInstanceOf[Seq[String]].distinct.size }
        , if (buffer(10).asInstanceOf[Seq[String]].isEmpty) {0.0} else { buffer(10).asInstanceOf[Seq[String]].distinct.size }
        , if (buffer(11).asInstanceOf[Seq[String]].isEmpty) {0.0} else { buffer(11).asInstanceOf[Seq[String]].distinct.size }
        , if (buffer(12).asInstanceOf[Seq[String]].isEmpty) {0.0} else { buffer(12).asInstanceOf[Seq[String]].distinct.size }
        , if (buffer(13).asInstanceOf[Seq[String]].isEmpty) {0.0} else { buffer(13).asInstanceOf[Seq[String]].distinct.size }
        , if (buffer(14).asInstanceOf[Seq[String]].isEmpty) {0.0} else { buffer(14).asInstanceOf[Seq[String]].distinct.size }
        , if (buffer(15).asInstanceOf[Seq[String]].isEmpty) {0.0} else { buffer(15).asInstanceOf[Seq[String]].distinct.size }
    )
  }
}

