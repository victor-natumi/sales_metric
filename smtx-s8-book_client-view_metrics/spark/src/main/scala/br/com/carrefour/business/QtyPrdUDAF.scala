package br.com.carrefour.business

import org.apache.spark.sql.Row
import org.apache.spark.sql.expressions.{MutableAggregationBuffer, UserDefinedAggregateFunction}
import org.apache.spark.sql.types._
import br.com.carrefour.model.UdafModel

class QtyPrdUDAF extends UserDefinedAggregateFunction with SalesMetricTrait {

  // Input Data Type Schema
  override def inputSchema: StructType = UdafModel.ClientSchema

  // Intermediate Schema
  override def bufferSchema: StructType = 
    StructType(
        StructField("produtos_total",DoubleType)
        :: StructField("produtos_store",DoubleType)
        :: StructField("produtos_hyp",  DoubleType)
        :: StructField("produtos_sup",  DoubleType)
        :: StructField("produtos_prx",  DoubleType)
        :: StructField("produtos_drg",  DoubleType)
        :: StructField("produtos_gas",  DoubleType)
        :: StructField("produtos_ecm",  DoubleType)
        :: StructField("produtos_std",  DoubleType)
        :: StructField("produtos_click",DoubleType)
        :: StructField("produtos_drive",DoubleType)
        :: StructField("produtos_scan", DoubleType)
        :: StructField("produtos_rappi",DoubleType)
        :: StructField("produtos_other",DoubleType)
        :: StructField("produtos_food", DoubleType)
        :: StructField("produtos_nfood",DoubleType)
        :: Nil
    )

  // Returned Data Type
  override def dataType: DataType = UdafModel.ResultSchema

  // Self-explaining
  override def deterministic: Boolean = true

  // This function is called whenever key changes
  override def initialize(buffer: MutableAggregationBuffer): Unit = {
    for (i <- 0 to buffer.size - 1) buffer(i) = 0.0
  }

  // Iterate over each entry of a group
  override def update(buffer: MutableAggregationBuffer, input: Row): Unit = {
    if (buffer != null) {
        val id_source_sale: Int       = input.get(idx_id_source_sale).asInstanceOf[Int]
        val ds_site_format: String    = input.get(idx_ds_site_format).asInstanceOf[String]
        val cod_ind_site_ecomm: Int   = input.get(idx_cod_ind_site_ecomm).asInstanceOf[Int]
        val lst_sales_items: Seq[Row] = input.get(idx_lst_sales_items).asInstanceOf[Seq[Row]]
        val cod_delivery: Int         = input.get(idx_cod_delivery).asInstanceOf[Int]
        val lst_cod_bus: Map[String, Double] = 
            lst_sales_items.
            filter(i => !i.isNullAt(idx_cod_bus) && !i.isNullAt(idx_cod_product_rms)).
            map(i => (
              i.get(idx_cod_bus).toString
              , Seq(i.get(idx_cod_product_rms).toString)
            )).
            groupBy(_._1).
            mapValues(s => s.map(_._2).reduce( _ ++ _ ).distinct.size)
        val cod_bus = lst_cod_bus.keys.toSeq
        val metric = new SalesMetricClassifier().check(id_source_sale, ds_site_format, cod_ind_site_ecomm, cod_delivery, cod_bus)
        //---------Sums--------------------
        buffer(0) =  if (metric("total"))  { buffer(0).asInstanceOf[Double] + lst_cod_bus.values.sum } else { buffer(0).asInstanceOf[Double] }
        buffer(1) =  if (metric("store"))  { buffer(1).asInstanceOf[Double] + lst_cod_bus.values.sum } else { buffer(1).asInstanceOf[Double] }
        buffer(2) =  if (metric("hyp"))    { buffer(2).asInstanceOf[Double] + lst_cod_bus.values.sum } else { buffer(2).asInstanceOf[Double] }
        buffer(3) =  if (metric("sup"))    { buffer(3).asInstanceOf[Double] + lst_cod_bus.values.sum } else { buffer(3).asInstanceOf[Double] }
        buffer(4) =  if (metric("prx"))    { buffer(4).asInstanceOf[Double] + lst_cod_bus.values.sum } else { buffer(4).asInstanceOf[Double] }
        buffer(5) =  if (metric("drg"))    { buffer(5).asInstanceOf[Double] + lst_cod_bus.values.sum } else { buffer(5).asInstanceOf[Double] }
        buffer(6) =  if (metric("gas"))    { buffer(6).asInstanceOf[Double] + lst_cod_bus.values.sum } else { buffer(6).asInstanceOf[Double] }
        buffer(7) =  if (metric("ecm"))    { buffer(7).asInstanceOf[Double] + lst_cod_bus.values.sum } else { buffer(7).asInstanceOf[Double] }
        buffer(8) =  if (metric("std"))    { buffer(8).asInstanceOf[Double] + lst_cod_bus.values.sum } else { buffer(8).asInstanceOf[Double] }
        buffer(9) =  if (metric("click"))  { buffer(9).asInstanceOf[Double] + lst_cod_bus.values.sum } else { buffer(9).asInstanceOf[Double] }
        buffer(10) =  if (metric("drive"))  { buffer(10).asInstanceOf[Double] + lst_cod_bus.values.sum } else { buffer(10).asInstanceOf[Double] }
        buffer(11) =  if (metric("scan"))   { buffer(11).asInstanceOf[Double] + lst_cod_bus.values.sum } else { buffer(11).asInstanceOf[Double] }
        buffer(12) =  if (metric("rappi"))  { buffer(12).asInstanceOf[Double] + lst_cod_bus.values.sum } else { buffer(12).asInstanceOf[Double] }
        buffer(13) =  if (metric("other"))  { buffer(13).asInstanceOf[Double] + lst_cod_bus.values.sum } else { buffer(13).asInstanceOf[Double] }
        buffer(14) =  if (metric("food"))   { buffer(14).asInstanceOf[Double] + lst_cod_bus.getOrElse("FOOD", 0.0) } else { buffer(14).asInstanceOf[Double] }
        buffer(15) =  if (metric("nfood"))  { buffer(15).asInstanceOf[Double] + lst_cod_bus.getOrElse("NFOOD", 0.0) } else { buffer(15).asInstanceOf[Double] }
    }
  }

  def merge(buffer1: MutableAggregationBuffer, buffer2: Row): Unit = {
    if (buffer1 != null && buffer2 != null) {
      for(i <- 0 to buffer1.size - 1) buffer1(i) = buffer1(i).asInstanceOf[Double] + buffer2(i).asInstanceOf[Double]
    }
  }
  // Called after all the entries are exhausted.
  override def evaluate(buffer: Row): Any = {
    val qty_prd_total   = buffer(0).asInstanceOf[Double]  
    val qty_prd_store   = buffer(1).asInstanceOf[Double]  
    val qty_prd_hyp     = buffer(2).asInstanceOf[Double]  
    val qty_prd_sup     = buffer(3).asInstanceOf[Double]  
    val qty_prd_prx     = buffer(4).asInstanceOf[Double]  
    val qty_prd_drg     = buffer(5).asInstanceOf[Double]  
    val qty_prd_gas     = buffer(6).asInstanceOf[Double]  
    val qty_prd_ecm     = buffer(7).asInstanceOf[Double]  
    val qty_prd_std     = buffer(8).asInstanceOf[Double]  
    val qty_prd_click   = buffer(9).asInstanceOf[Double]  
    val qty_prd_drive   = buffer(10).asInstanceOf[Double] 
    val qty_prd_scan    = buffer(11).asInstanceOf[Double] 
    val qty_prd_rappi   = buffer(12).asInstanceOf[Double] 
    val qty_prd_other   = buffer(13).asInstanceOf[Double] 
    val qty_prd_food    = buffer(14).asInstanceOf[Double] 
    val qty_prd_nfood   = buffer(15).asInstanceOf[Double] 
    Seq(
        if (qty_prd_total.isNaN)    {0.0} else {qty_prd_total}
        , if (qty_prd_store.isNaN)  {0.0} else {qty_prd_store}
        , if (qty_prd_hyp.isNaN)    {0.0} else {qty_prd_hyp}
        , if (qty_prd_sup.isNaN)    {0.0} else {qty_prd_sup}
        , if (qty_prd_prx.isNaN)    {0.0} else {qty_prd_prx}
        , if (qty_prd_drg.isNaN)    {0.0} else {qty_prd_drg}
        , if (qty_prd_gas.isNaN)    {0.0} else {qty_prd_gas}
        , if (qty_prd_ecm.isNaN)    {0.0} else {qty_prd_ecm}
        , if (qty_prd_std.isNaN)    {0.0} else {qty_prd_std}
        , if (qty_prd_click.isNaN)  {0.0} else {qty_prd_click}
        , if (qty_prd_drive.isNaN)  {0.0} else {qty_prd_drive}
        , if (qty_prd_scan.isNaN)   {0.0} else {qty_prd_scan}
        , if (qty_prd_rappi.isNaN)  {0.0} else {qty_prd_rappi}
        , if (qty_prd_other.isNaN)  {0.0} else {qty_prd_other}
        , if (qty_prd_food.isNaN)   {0.0} else {qty_prd_food}
        , if (qty_prd_nfood.isNaN)  {0.0} else {qty_prd_nfood}
    )
  }
}

