package br.com.carrefour.service

// built-in
import org.slf4j.LoggerFactory
// thrid party
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._
import org.apache.spark.sql.{DataFrame, Row, SparkSession}
import org.apache.spark.storage.StorageLevel
// local
import br.com.carrefour.business.SalesMetricBusiness
import br.com.carrefour.dao._
import br.com.carrefour.model._
import br.com.carrefour.utils._

object SalesService {

  val log = LoggerFactory.getLogger(this.getClass)

  def saveMetric(spark:SparkSession, date:String, metrics:String, paramView:String) = {
    val businessInstance: SalesMetricBusiness = SalesMetricBusiness.getInstance(date, paramView)
    val dataFrame: DataFrame = paramView match {
      case "6" => {
        businessInstance.transform_for_format(
          SalesDao.getByMonth(spark, date).
          withColumn("salt", (rand * ConfigUtil.getSparkPartitions).cast(IntegerType)).
          repartition(ConfigUtil.getSparkPartitions, col(SalesModel.Field.DS_SITE_FORMAT), col("salt"))
        )
      }
      case "8" => {
        businessInstance.transformDF(
          SalesDao.getByMonth(spark, date).
          withColumn("salt", (rand * ConfigUtil.getSparkPartitions).cast(IntegerType)).
          repartition(ConfigUtil.getSparkPartitions, col(SalesModel.Field.COD_UF), col("salt"))
        )
      }
      case "11" => {
        businessInstance.transform_for_client_registered(SalesDao.getByMonth(spark, date)).
        repartition(ConfigUtil.getSparkPartitions, col(SalesModel.Field.NUM_CONSUMER_DOCUMENT_PRIORITY))
      }
      case _ => {
        businessInstance.transformDF(SalesDao.getByMonth(spark, date)).
        repartition(ConfigUtil.getSparkPartitions, col(SalesModel.Field.NUM_CONSUMER_DOCUMENT_PRIORITY))
      }
    }

    val list_metrics: String = if (metrics.isEmpty) { 
      MetricsModel.code_metrics.map{ m => m._1.toInt}.toSeq.sorted.map{ m => s"${m}"}.mkString(",")
    } else { metrics }

    val metricsDF: DataFrame = {
      val dfReturn: Seq[DataFrame] = list_metrics.split(",").map{
        x => {
          log.info(s"[*] Executing metric: ${MetricsModel.code_metrics(x)}")
          businessInstance.applyMetric(dataFrame, x)
        }
      }
      dfReturn.reduce(_.union(_))
    }

    log.info(s"[*] Saving to Hive")
    MetricsDao.save(
      spark
      , businessInstance.getResult(metricsDF)
      , date
      , paramView
    )
  }
}
