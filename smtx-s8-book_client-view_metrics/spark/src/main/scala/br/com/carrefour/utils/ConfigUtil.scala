package br.com.carrefour.utils

import com.typesafe.config.{ConfigObject, ConfigValue, ConfigFactory, Config}
import scala.collection.JavaConverters._  
import java.util.Map.Entry

object ConfigUtil {
  private val configManager = ConfigFactory.load(this.getClass().getClassLoader())

  def getMap(config: String): Map[String, String] = {
    val list : Iterable[ConfigObject] = configManager.getObjectList(config).asScala
    (
      for {
        item : ConfigObject <- list
        entry : Entry[String, ConfigValue] <- item.entrySet().asScala
        key = entry.getKey
        value = entry.getValue.unwrapped().toString
      } yield (key, value)
    ).toMap
  }
  
  def getSparkPartitions: Int = {
    configManager.getInt("spark.numrepartitions")
  }

  def getHiveWarehouse: String = {
    configManager.getString("hive.warehouse")
  }

} 
