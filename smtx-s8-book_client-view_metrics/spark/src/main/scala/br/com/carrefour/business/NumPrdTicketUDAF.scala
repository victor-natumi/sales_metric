package br.com.carrefour.business

import org.apache.spark.sql.Row
import org.apache.spark.sql.expressions.{MutableAggregationBuffer, UserDefinedAggregateFunction}
import org.apache.spark.sql.types._
import br.com.carrefour.model.UdafModel

class NumPrdTicketUDAF extends UserDefinedAggregateFunction with SalesMetricTrait {

  // Input Data Type Schema
  override def inputSchema: StructType = UdafModel.ClientSchema

  // Intermediate Schema
  override def bufferSchema: StructType = 
    StructType(
        StructField("compras_total", DoubleType)
        :: StructField("compras_store", DoubleType)
        :: StructField("compras_hyp", DoubleType)
        :: StructField("compras_sup", DoubleType)
        :: StructField("compras_prx", DoubleType)
        :: StructField("compras_drg", DoubleType)
        :: StructField("compras_gas", DoubleType)
        :: StructField("compras_ecm", DoubleType)
        :: StructField("compras_std", DoubleType)
        :: StructField("compras_click", DoubleType)
        :: StructField("compras_drive", DoubleType)
        :: StructField("compras_scan", DoubleType)
        :: StructField("compras_rappi", DoubleType)
        :: StructField("compras_other", DoubleType)
        :: StructField("compras_food", DoubleType)
        :: StructField("compras_nfood", DoubleType)
        :: StructField("produtos_total", DoubleType)
        :: StructField("produtos_store", DoubleType)
        :: StructField("produtos_hyp", DoubleType)
        :: StructField("produtos_sup", DoubleType)
        :: StructField("produtos_prx", DoubleType)
        :: StructField("produtos_drg", DoubleType)
        :: StructField("produtos_gas", DoubleType)
        :: StructField("produtos_ecm", DoubleType)
        :: StructField("produtos_std", DoubleType)
        :: StructField("produtos_click", DoubleType)
        :: StructField("produtos_drive", DoubleType)
        :: StructField("produtos_scan", DoubleType)
        :: StructField("produtos_rappi", DoubleType)
        :: StructField("produtos_other", DoubleType)
        :: StructField("produtos_food", DoubleType)
        :: StructField("produtos_nfood", DoubleType)
        :: Nil
    )

  // Returned Data Type
  override def dataType: DataType = UdafModel.ResultSchema

  // Self-explaining
  override def deterministic: Boolean = true

  // This function is called whenever key changes
  override def initialize(buffer: MutableAggregationBuffer): Unit = {
    for (i <- 0 to buffer.size - 1) buffer(i) = 0.0
  }

  // Iterate over each entry of a group
  override def update(buffer: MutableAggregationBuffer, input: Row): Unit = {
    if(buffer != null) {
        val id_source_sale: Int       = input.get(idx_id_source_sale).asInstanceOf[Int]
        val ds_site_format: String    = input.get(idx_ds_site_format).asInstanceOf[String]
        val cod_ind_site_ecomm: Int   = input.get(idx_cod_ind_site_ecomm).asInstanceOf[Int]
        val lst_sales_items: Seq[Row] = input.get(idx_lst_sales_items).asInstanceOf[Seq[Row]]
        val cod_delivery: Int         = input.get(idx_cod_delivery).asInstanceOf[Int]
        val lst_cod_bus: Map[String, Double] = 
            lst_sales_items.
            filter(i => !i.isNullAt(idx_cod_bus) && !i.isNullAt(idx_cod_product_rms)).
            map(i => (
              i.get(idx_cod_bus).toString
              , Seq(i.get(idx_cod_product_rms).toString)
            )).
            groupBy(_._1).
            mapValues(s => s.map(_._2).reduce( _ ++ _ ).distinct.size)
        val cod_bus = lst_cod_bus.keys.toSeq
        val metric = new SalesMetricClassifier().check(id_source_sale, ds_site_format, cod_ind_site_ecomm, cod_delivery, cod_bus)
        //---------Counts------------------
        buffer(0) =  if (metric("total"))  { buffer(0).asInstanceOf[Double] + 1.0 } else { buffer(0).asInstanceOf[Double] }
        buffer(1) =  if (metric("store"))  { buffer(1).asInstanceOf[Double] + 1.0 } else { buffer(1).asInstanceOf[Double] } 
        buffer(2) =  if (metric("hyp"))    { buffer(2).asInstanceOf[Double] + 1.0 } else { buffer(2).asInstanceOf[Double] }
        buffer(3) =  if (metric("sup"))    { buffer(3).asInstanceOf[Double] + 1.0 } else { buffer(3).asInstanceOf[Double] }
        buffer(4) =  if (metric("prx"))    { buffer(4).asInstanceOf[Double] + 1.0 } else { buffer(4).asInstanceOf[Double] }
        buffer(5) =  if (metric("drg"))    { buffer(5).asInstanceOf[Double] + 1.0 } else { buffer(5).asInstanceOf[Double] }
        buffer(6) =  if (metric("gas"))    { buffer(6).asInstanceOf[Double] + 1.0 } else { buffer(6).asInstanceOf[Double] }
        buffer(7) =  if (metric("ecm"))    { buffer(7).asInstanceOf[Double] + 1.0 } else { buffer(7).asInstanceOf[Double] }
        buffer(8) =  if (metric("std"))    { buffer(8).asInstanceOf[Double] + 1.0 } else { buffer(8).asInstanceOf[Double] }
        buffer(9) =  if (metric("click"))  { buffer(9).asInstanceOf[Double] + 1.0 } else { buffer(9).asInstanceOf[Double] }
        buffer(10) =  if (metric("drive"))  { buffer(10).asInstanceOf[Double] + 1.0 } else { buffer(10).asInstanceOf[Double] }
        buffer(11) =  if (metric("scan"))   { buffer(11).asInstanceOf[Double] + 1.0 } else { buffer(11).asInstanceOf[Double] }
        buffer(12) =  if (metric("rappi"))  { buffer(12).asInstanceOf[Double] + 1.0 } else { buffer(12).asInstanceOf[Double] }
        buffer(13) =  if (metric("other"))  { buffer(13).asInstanceOf[Double] + 1.0 } else { buffer(13).asInstanceOf[Double] }
        buffer(14) =  if (metric("food"))   { buffer(14).asInstanceOf[Double] + 1.0 } else { buffer(14).asInstanceOf[Double] }
        buffer(15) =  if (metric("nfood"))  { buffer(15).asInstanceOf[Double] + 1.0 } else { buffer(15).asInstanceOf[Double] }
        //---------Sums--------------------
        buffer(16) =  if (metric("total"))  { buffer(16).asInstanceOf[Double] + lst_cod_bus.values.sum } else { buffer(16).asInstanceOf[Double] }
        buffer(17) =  if (metric("store"))  { buffer(17).asInstanceOf[Double] + lst_cod_bus.values.sum } else { buffer(17).asInstanceOf[Double] }
        buffer(18) =  if (metric("hyp"))    { buffer(18).asInstanceOf[Double] + lst_cod_bus.values.sum } else { buffer(18).asInstanceOf[Double] }
        buffer(19) =  if (metric("sup"))    { buffer(19).asInstanceOf[Double] + lst_cod_bus.values.sum } else { buffer(19).asInstanceOf[Double] }
        buffer(20) =  if (metric("prx"))    { buffer(20).asInstanceOf[Double] + lst_cod_bus.values.sum } else { buffer(20).asInstanceOf[Double] }
        buffer(21) =  if (metric("drg"))    { buffer(21).asInstanceOf[Double] + lst_cod_bus.values.sum } else { buffer(21).asInstanceOf[Double] }
        buffer(22) =  if (metric("gas"))    { buffer(22).asInstanceOf[Double] + lst_cod_bus.values.sum } else { buffer(22).asInstanceOf[Double] }
        buffer(23) =  if (metric("ecm"))    { buffer(23).asInstanceOf[Double] + lst_cod_bus.values.sum } else { buffer(23).asInstanceOf[Double] }
        buffer(24) =  if (metric("std"))    { buffer(24).asInstanceOf[Double] + lst_cod_bus.values.sum } else { buffer(24).asInstanceOf[Double] }
        buffer(25) =  if (metric("click"))  { buffer(25).asInstanceOf[Double] + lst_cod_bus.values.sum } else { buffer(25).asInstanceOf[Double] }
        buffer(26) =  if (metric("drive"))  { buffer(26).asInstanceOf[Double] + lst_cod_bus.values.sum } else { buffer(26).asInstanceOf[Double] }
        buffer(27) =  if (metric("scan"))   { buffer(27).asInstanceOf[Double] + lst_cod_bus.values.sum } else { buffer(27).asInstanceOf[Double] }
        buffer(28) =  if (metric("rappi"))  { buffer(28).asInstanceOf[Double] + lst_cod_bus.values.sum } else { buffer(28).asInstanceOf[Double] }
        buffer(29) =  if (metric("other"))  { buffer(29).asInstanceOf[Double] + lst_cod_bus.values.sum } else { buffer(29).asInstanceOf[Double] }
        buffer(30) =  if (metric("food"))   { buffer(30).asInstanceOf[Double] + lst_cod_bus.getOrElse("FOOD", 0.0) } else { buffer(30).asInstanceOf[Double] }
        buffer(31) =  if (metric("nfood"))  { buffer(31).asInstanceOf[Double] + lst_cod_bus.getOrElse("NFOOD", 0.0) } else { buffer(31).asInstanceOf[Double] } 
    }
  }

  def merge(buffer1: MutableAggregationBuffer, buffer2: Row): Unit = {
    if (buffer1 != null && buffer2 != null) {
      for(i <- 0 to buffer1.size - 1) buffer1(i) = buffer1(i).asInstanceOf[Double] + buffer2(i).asInstanceOf[Double]
    }
  }
  // Called after all the entries are exhausted.
  override def evaluate(buffer: Row): Any = {
    val num_prd_ticket_total   = buffer(16).asInstanceOf[Double] / buffer(0).asInstanceOf[Double]  
    val num_prd_ticket_store   = buffer(17).asInstanceOf[Double] / buffer(1).asInstanceOf[Double]  
    val num_prd_ticket_hyp     = buffer(18).asInstanceOf[Double] / buffer(2).asInstanceOf[Double]  
    val num_prd_ticket_sup     = buffer(19).asInstanceOf[Double] / buffer(3).asInstanceOf[Double]  
    val num_prd_ticket_prx     = buffer(20).asInstanceOf[Double] / buffer(4).asInstanceOf[Double]  
    val num_prd_ticket_drg     = buffer(21).asInstanceOf[Double] / buffer(5).asInstanceOf[Double]  
    val num_prd_ticket_gas     = buffer(22).asInstanceOf[Double] / buffer(6).asInstanceOf[Double]  
    val num_prd_ticket_ecm     = buffer(23).asInstanceOf[Double] / buffer(7).asInstanceOf[Double]  
    val num_prd_ticket_std     = buffer(24).asInstanceOf[Double] / buffer(8).asInstanceOf[Double]  
    val num_prd_ticket_click   = buffer(25).asInstanceOf[Double] / buffer(9).asInstanceOf[Double]  
    val num_prd_ticket_drive   = buffer(26).asInstanceOf[Double] / buffer(10).asInstanceOf[Double] 
    val num_prd_ticket_scan    = buffer(27).asInstanceOf[Double] / buffer(11).asInstanceOf[Double] 
    val num_prd_ticket_rappi   = buffer(28).asInstanceOf[Double] / buffer(12).asInstanceOf[Double] 
    val num_prd_ticket_other   = buffer(29).asInstanceOf[Double] / buffer(13).asInstanceOf[Double] 
    val num_prd_ticket_food    = buffer(30).asInstanceOf[Double] / buffer(14).asInstanceOf[Double] 
    val num_prd_ticket_nfood   = buffer(31).asInstanceOf[Double] / buffer(15).asInstanceOf[Double] 
    Seq(
        if (num_prd_ticket_total.isNaN)    {0.0} else {num_prd_ticket_total}
        , if (num_prd_ticket_store.isNaN)  {0.0} else {num_prd_ticket_store}
        , if (num_prd_ticket_hyp.isNaN)    {0.0} else {num_prd_ticket_hyp}
        , if (num_prd_ticket_sup.isNaN)    {0.0} else {num_prd_ticket_sup}
        , if (num_prd_ticket_prx.isNaN)    {0.0} else {num_prd_ticket_prx}
        , if (num_prd_ticket_drg.isNaN)    {0.0} else {num_prd_ticket_drg}
        , if (num_prd_ticket_gas.isNaN)    {0.0} else {num_prd_ticket_gas}
        , if (num_prd_ticket_ecm.isNaN)    {0.0} else {num_prd_ticket_ecm}
        , if (num_prd_ticket_std.isNaN)    {0.0} else {num_prd_ticket_std}
        , if (num_prd_ticket_click.isNaN)  {0.0} else {num_prd_ticket_click}
        , if (num_prd_ticket_drive.isNaN)  {0.0} else {num_prd_ticket_drive}
        , if (num_prd_ticket_scan.isNaN)   {0.0} else {num_prd_ticket_scan}
        , if (num_prd_ticket_rappi.isNaN)  {0.0} else {num_prd_ticket_rappi}
        , if (num_prd_ticket_other.isNaN)  {0.0} else {num_prd_ticket_other}
        , if (num_prd_ticket_food.isNaN)   {0.0} else {num_prd_ticket_food}
        , if (num_prd_ticket_nfood.isNaN)  {0.0} else {num_prd_ticket_nfood}
    )
  }
}

