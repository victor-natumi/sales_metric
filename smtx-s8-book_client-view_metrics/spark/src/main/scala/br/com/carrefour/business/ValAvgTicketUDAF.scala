package br.com.carrefour.business

import org.apache.spark.sql.Row
import org.apache.spark.sql.expressions.{MutableAggregationBuffer, UserDefinedAggregateFunction}
import org.apache.spark.sql.types._
import br.com.carrefour.model.UdafModel

class ValAvgTicketUDAF extends UserDefinedAggregateFunction with SalesMetricTrait{
  override def inputSchema: StructType = UdafModel.ClientSchema

  override def bufferSchema: StructType = {
    StructType(
      StructField("count_val_document_total", DoubleType)
        :: StructField("count_val_document_total_store", DoubleType)
        :: StructField("count_val_document_total_hyp", DoubleType)
        :: StructField("count_val_document_total_sup", DoubleType)
        :: StructField("count_val_document_total_prx", DoubleType)
        :: StructField("count_val_document_total_drg", DoubleType)
        :: StructField("count_val_document_total_gas", DoubleType)
        :: StructField("count_val_document_total_ecom", DoubleType)
        :: StructField("count_val_document_total_std", DoubleType)
        :: StructField("count_val_document_total_click", DoubleType)
        :: StructField("count_val_document_total_drive", DoubleType)
        :: StructField("count_val_document_total_scan", DoubleType)
        :: StructField("count_val_document_total_rappi", DoubleType)
        :: StructField("count_val_document_total_other", DoubleType)
        :: StructField("count_val_document_total_food", DoubleType)
        :: StructField("count_val_document_total_nfood", DoubleType)
        :: StructField("sum_val_document_total", DoubleType)
        :: StructField("sum_val_document_total_store", DoubleType)
        :: StructField("sum_val_document_total_hyp", DoubleType)
        :: StructField("sum_val_document_total_sup", DoubleType)
        :: StructField("sum_val_document_total_prx", DoubleType)
        :: StructField("sum_val_document_total_drg", DoubleType)
        :: StructField("sum_val_document_total_gas", DoubleType)
        :: StructField("sum_val_document_total_ecom", DoubleType)
        :: StructField("sum_val_document_total_std", DoubleType)
        :: StructField("sum_val_document_total_click", DoubleType)
        :: StructField("sum_val_document_total_drive", DoubleType)
        :: StructField("sum_val_document_total_scan", DoubleType)
        :: StructField("sum_val_document_total_rappi", DoubleType)
        :: StructField("sum_val_document_total_other", DoubleType)
        :: StructField("sum_val_document_total_food", DoubleType)
        :: StructField("sum_val_document_total_nfood", DoubleType)
        :: Nil
    )
  }

  override def dataType: DataType = UdafModel.ResultSchema

  override def deterministic: Boolean = true

  override def initialize(buffer: MutableAggregationBuffer): Unit = {
    for (i <- 0 to buffer.size - 1) { buffer(i) = 0.0 }
  }

  override def update(buffer: MutableAggregationBuffer, input: Row): Unit = {
    if (buffer != null) {
      val id_source_sale: Int       = input.get(idx_id_source_sale).asInstanceOf[Int]
      val ds_site_format: String    = input.get(idx_ds_site_format).asInstanceOf[String]
      val cod_ind_site_ecomm: Int   = input.get(idx_cod_ind_site_ecomm).asInstanceOf[Int]
      val lst_sales_items: Seq[Row] = input.get(idx_lst_sales_items).asInstanceOf[Seq[Row]]
      val cod_delivery: Int         = input.get(idx_cod_delivery).asInstanceOf[Int]
      val lst_cod_bus: Map[String, Double] = 
          lst_sales_items.
          filter(i => !i.isNullAt(idx_cod_bus)).
          map(i => (
            i.get(idx_cod_bus).toString 
            , i.get(idx_val_total_with_discount).asInstanceOf[java.math.BigDecimal].doubleValue()
          )).
          groupBy(_._1).
          mapValues(s => s.map(_._2).reduce(_ + _))
      val cod_bus = lst_cod_bus.keys.toSeq
      val metric = new SalesMetricClassifier().check(id_source_sale, ds_site_format, cod_ind_site_ecomm, cod_delivery, cod_bus)
      val val_document_total_with_services: Double = input.get(idx_val_document_total_with_services).asInstanceOf[java.math.BigDecimal].doubleValue()
      //---------Counts------------------
      buffer(0) =  if (metric("total"))  { buffer(0).asInstanceOf[Double] + 1.0 } else { buffer(0).asInstanceOf[Double] }
      buffer(1) =  if (metric("store"))  { buffer(1).asInstanceOf[Double] + 1.0 } else { buffer(1).asInstanceOf[Double] } 
      buffer(2) =  if (metric("hyp"))    { buffer(2).asInstanceOf[Double] + 1.0 } else { buffer(2).asInstanceOf[Double] }
      buffer(3) =  if (metric("sup"))    { buffer(3).asInstanceOf[Double] + 1.0 } else { buffer(3).asInstanceOf[Double] }
      buffer(4) =  if (metric("prx"))    { buffer(4).asInstanceOf[Double] + 1.0 } else { buffer(4).asInstanceOf[Double] }
      buffer(5) =  if (metric("drg"))    { buffer(5).asInstanceOf[Double] + 1.0 } else { buffer(5).asInstanceOf[Double] }
      buffer(6) =  if (metric("gas"))    { buffer(6).asInstanceOf[Double] + 1.0 } else { buffer(6).asInstanceOf[Double] }
      buffer(7) =  if (metric("ecm"))    { buffer(7).asInstanceOf[Double] + 1.0 } else { buffer(7).asInstanceOf[Double] }
      buffer(8) =  if (metric("std"))    { buffer(8).asInstanceOf[Double] + 1.0 } else { buffer(8).asInstanceOf[Double] }
      buffer(9) =  if (metric("click"))  { buffer(9).asInstanceOf[Double] + 1.0 } else { buffer(9).asInstanceOf[Double] }
      buffer(10) =  if (metric("drive"))  { buffer(10).asInstanceOf[Double] + 1.0 } else { buffer(10).asInstanceOf[Double] }
      buffer(11) =  if (metric("scan"))   { buffer(11).asInstanceOf[Double] + 1.0 } else { buffer(11).asInstanceOf[Double] }
      buffer(12) =  if (metric("rappi"))  { buffer(12).asInstanceOf[Double] + 1.0 } else { buffer(12).asInstanceOf[Double] }
      buffer(13) =  if (metric("other"))  { buffer(13).asInstanceOf[Double] + 1.0 } else { buffer(13).asInstanceOf[Double] }
      buffer(14) =  if (metric("food"))   { buffer(14).asInstanceOf[Double] + 1.0 } else { buffer(14).asInstanceOf[Double] }
      buffer(15) =  if (metric("nfood"))  { buffer(15).asInstanceOf[Double] + 1.0 } else { buffer(15).asInstanceOf[Double] }
      //---------Sums--------------------
      buffer(16) =  if (metric("total"))  { buffer(16).asInstanceOf[Double] + lst_cod_bus.values.sum } else { buffer(16).asInstanceOf[Double] }
      buffer(17) =  if (metric("store"))  { buffer(17).asInstanceOf[Double] + lst_cod_bus.values.sum } else { buffer(17).asInstanceOf[Double] }
      buffer(18) =  if (metric("hyp"))    { buffer(18).asInstanceOf[Double] + lst_cod_bus.values.sum } else { buffer(18).asInstanceOf[Double] }
      buffer(19) =  if (metric("sup"))    { buffer(19).asInstanceOf[Double] + lst_cod_bus.values.sum } else { buffer(19).asInstanceOf[Double] }
      buffer(20) =  if (metric("prx"))    { buffer(20).asInstanceOf[Double] + lst_cod_bus.values.sum } else { buffer(20).asInstanceOf[Double] }
      buffer(21) =  if (metric("drg"))    { buffer(21).asInstanceOf[Double] + lst_cod_bus.values.sum } else { buffer(21).asInstanceOf[Double] }
      buffer(22) =  if (metric("gas"))    { buffer(22).asInstanceOf[Double] + lst_cod_bus.values.sum } else { buffer(22).asInstanceOf[Double] }
      buffer(23) =  if (metric("ecm"))    { buffer(23).asInstanceOf[Double] + lst_cod_bus.values.sum } else { buffer(23).asInstanceOf[Double] }
      buffer(24) =  if (metric("std"))    { buffer(24).asInstanceOf[Double] + lst_cod_bus.values.sum } else { buffer(24).asInstanceOf[Double] }
      buffer(25) =  if (metric("click"))  { buffer(25).asInstanceOf[Double] + lst_cod_bus.values.sum } else { buffer(25).asInstanceOf[Double] }
      buffer(26) =  if (metric("drive"))  { buffer(26).asInstanceOf[Double] + lst_cod_bus.values.sum } else { buffer(26).asInstanceOf[Double] }
      buffer(27) =  if (metric("scan"))   { buffer(27).asInstanceOf[Double] + lst_cod_bus.values.sum } else { buffer(27).asInstanceOf[Double] }
      buffer(28) =  if (metric("rappi"))  { buffer(28).asInstanceOf[Double] + lst_cod_bus.values.sum } else { buffer(28).asInstanceOf[Double] }
      buffer(29) =  if (metric("other"))  { buffer(29).asInstanceOf[Double] + lst_cod_bus.values.sum } else { buffer(29).asInstanceOf[Double] }
      buffer(30) =  if (metric("food"))   { buffer(30).asInstanceOf[Double] + lst_cod_bus.getOrElse("FOOD", 0.0) } else { buffer(30).asInstanceOf[Double] }
      buffer(31) =  if (metric("nfood"))  { buffer(31).asInstanceOf[Double] + lst_cod_bus.getOrElse("NFOOD", 0.0) } else { buffer(31).asInstanceOf[Double] } 
    }

  }

  override def merge(buffer1: MutableAggregationBuffer, buffer2: Row): Unit = {
    if (buffer1 != null && buffer2 != null) {
      for(i <- 0 to buffer1.size - 1) buffer1(i) = buffer1(i).asInstanceOf[Double] + buffer2(i).asInstanceOf[Double]
    }
  }

  override def evaluate(buffer: Row): Any = {
    val val_avg_ticket_total = buffer(16).asInstanceOf[Double] / buffer(0).asInstanceOf[Double]
    val val_avg_ticket_store = buffer(17).asInstanceOf[Double] / buffer(1).asInstanceOf[Double]
    val val_avg_ticket_hyp = buffer(18).asInstanceOf[Double] / buffer(2).asInstanceOf[Double]
    val val_avg_ticket_sup = buffer(19).asInstanceOf[Double] / buffer(3).asInstanceOf[Double]
    val val_avg_ticket_prx = buffer(20).asInstanceOf[Double] / buffer(4).asInstanceOf[Double]
    val val_avg_ticket_drg = buffer(21).asInstanceOf[Double] / buffer(5).asInstanceOf[Double]
    val val_avg_ticket_gas = buffer(22).asInstanceOf[Double] / buffer(6).asInstanceOf[Double]
    val val_avg_ticket_ecm = buffer(23).asInstanceOf[Double] / buffer(7).asInstanceOf[Double]
    val val_avg_ticket_std = buffer(24).asInstanceOf[Double] / buffer(8).asInstanceOf[Double]
    val val_avg_ticket_click = buffer(25).asInstanceOf[Double] / buffer(9).asInstanceOf[Double]
    val val_avg_ticket_drive = buffer(26).asInstanceOf[Double] / buffer(10).asInstanceOf[Double]
    val val_avg_ticket_scan = buffer(27).asInstanceOf[Double] / buffer(11).asInstanceOf[Double]
    val val_avg_ticket_rappi = buffer(28).asInstanceOf[Double] / buffer(12).asInstanceOf[Double]
    val val_avg_ticket_other = buffer(29).asInstanceOf[Double] / buffer(13).asInstanceOf[Double]
    val val_avg_ticket_food = buffer(30).asInstanceOf[Double] / buffer(14).asInstanceOf[Double]
    val val_avg_ticket_nfood = buffer(31).asInstanceOf[Double] / buffer(15).asInstanceOf[Double]
    Seq(
      if (val_avg_ticket_total.isNaN)    { 0.0 } else { val_avg_ticket_total }
      , if (val_avg_ticket_store.isNaN)  { 0.0 } else { val_avg_ticket_store }
      , if (val_avg_ticket_hyp.isNaN)    { 0.0 } else { val_avg_ticket_hyp }
      , if (val_avg_ticket_sup.isNaN)    { 0.0 } else { val_avg_ticket_sup }
      , if (val_avg_ticket_prx.isNaN)    { 0.0 } else { val_avg_ticket_prx }
      , if (val_avg_ticket_drg.isNaN)    { 0.0 } else { val_avg_ticket_drg }
      , if (val_avg_ticket_gas.isNaN)    { 0.0 } else { val_avg_ticket_gas }
      , if (val_avg_ticket_ecm.isNaN)    { 0.0 } else { val_avg_ticket_ecm }
      , if (val_avg_ticket_std.isNaN)    { 0.0 } else { val_avg_ticket_std }
      , if (val_avg_ticket_click.isNaN)  { 0.0 } else { val_avg_ticket_click }
      , if (val_avg_ticket_drive.isNaN)  { 0.0 } else { val_avg_ticket_drive }
      , if (val_avg_ticket_scan.isNaN)   { 0.0 } else { val_avg_ticket_scan }
      , if (val_avg_ticket_rappi.isNaN)  { 0.0 } else { val_avg_ticket_rappi }
      , if (val_avg_ticket_other.isNaN)  { 0.0 } else { val_avg_ticket_other }
      , if (val_avg_ticket_food.isNaN)   { 0.0 } else { val_avg_ticket_food }
      , if (val_avg_ticket_nfood.isNaN)  { 0.0 } else { val_avg_ticket_nfood }
    )
  }
}
