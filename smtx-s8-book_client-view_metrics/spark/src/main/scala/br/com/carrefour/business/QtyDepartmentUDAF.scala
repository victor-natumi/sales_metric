package br.com.carrefour.business

import br.com.carrefour.model.UdafModel
import org.apache.spark.sql.Row
import org.apache.spark.sql.expressions.{MutableAggregationBuffer, UserDefinedAggregateFunction}
import org.apache.spark.sql.types._

class QtyDepartmentUDAF extends UserDefinedAggregateFunction with SalesMetricTrait {

  // Input Data Type Schema
  override def inputSchema: StructType = UdafModel.ClientSchema

  // Intermediate Schema
  override def bufferSchema: StructType =
    StructType(
      StructField("total", DoubleType)
      :: StructField("store", DoubleType)
      :: StructField("hyp", DoubleType)
      :: StructField("sup", DoubleType)
      :: StructField("prx", DoubleType)
      :: StructField("drg", DoubleType)
      :: StructField("gas", DoubleType)
      :: StructField("ecm", DoubleType)
      :: StructField("std", DoubleType)
      :: StructField("click", DoubleType)
      :: StructField("drive", DoubleType)
      :: StructField("scan", DoubleType)
      :: StructField("rappi", DoubleType)
      :: StructField("other", DoubleType)
      :: StructField("food", DoubleType)
      :: StructField("nfood", DoubleType)
      :: Nil
    )

  // Returned Data Type
  override def dataType: DataType = UdafModel.ResultSchema

  // Self-explaining
  override def deterministic: Boolean = true

  // This function is called whenever key changes
  override def initialize(buffer: MutableAggregationBuffer): Unit = {
    for (i <- 0 until buffer.size) buffer(i) = 0.0
  }

  // Iterate over each entry of a group
  override def update(buffer: MutableAggregationBuffer, input: Row): Unit = {
    if(buffer != null) {
      val id_source_sale: Int = input.get(idx_id_source_sale).asInstanceOf[Int]
      val ds_site_format: String = input.get(idx_ds_site_format).asInstanceOf[String]
      val cod_ind_site_ecomm: Int = input.get(idx_cod_ind_site_ecomm).asInstanceOf[Int]
      val cod_delivery: Int = input.get(idx_cod_delivery).asInstanceOf[Int]
      val lst_sales_items: Seq[Row] = input.get(idx_lst_sales_items).asInstanceOf[Seq[Row]]
      val cod_bus: Seq[String] = lst_sales_items.filter(i => !i.isNullAt(idx_cod_bus)).map(i => i.get(idx_cod_bus).toString).distinct

      val itemsF =
        lst_sales_items.
          filter(i => !i.isNullAt(idx_cod_bus) && !i.isNullAt(idx_ds_sector)).
          map(i => (
            i.get(idx_cod_bus).toString
            , Seq(i.get(idx_ds_sector))
          ))

      val lst_cod_bus: Map[String, Double] = itemsF.groupBy(_._1).mapValues(s => s.map(_._2).reduce(_ ++ _).distinct.size)
      val groupAmount = itemsF.map(i => i._2).distinct.size

      val metric = new SalesMetricClassifier().check(id_source_sale, ds_site_format, cod_ind_site_ecomm, cod_delivery, cod_bus)
        //---------Counts------------------
        buffer(0) =  if (metric("total"))  { buffer(0).asInstanceOf[Double] + groupAmount } else { buffer(0).asInstanceOf[Double] }
        buffer(1) =  if (metric("store"))  { buffer(1).asInstanceOf[Double] + groupAmount } else { buffer(1).asInstanceOf[Double] }
        buffer(2) =  if (metric("hyp"))    { buffer(2).asInstanceOf[Double] + groupAmount } else { buffer(2).asInstanceOf[Double] }
        buffer(3) =  if (metric("sup"))    { buffer(3).asInstanceOf[Double] + groupAmount } else { buffer(3).asInstanceOf[Double] }
        buffer(4) =  if (metric("prx"))    { buffer(4).asInstanceOf[Double] + groupAmount } else { buffer(4).asInstanceOf[Double] }
        buffer(5) =  if (metric("drg"))    { buffer(5).asInstanceOf[Double] + groupAmount } else { buffer(5).asInstanceOf[Double] }
        buffer(6) =  if (metric("gas"))    { buffer(6).asInstanceOf[Double] + groupAmount } else { buffer(6).asInstanceOf[Double] }
        buffer(7) =  if (metric("ecm"))    { buffer(7).asInstanceOf[Double] + groupAmount } else { buffer(7).asInstanceOf[Double] }
        buffer(8) =  if (metric("std"))    { buffer(8).asInstanceOf[Double] + groupAmount } else { buffer(8).asInstanceOf[Double] }
        buffer(9) =  if (metric("click"))  { buffer(9).asInstanceOf[Double] + groupAmount } else { buffer(9).asInstanceOf[Double] }
        buffer(10) =  if (metric("drive"))  { buffer(10).asInstanceOf[Double] + groupAmount } else { buffer(10).asInstanceOf[Double] }
        buffer(11) =  if (metric("scan"))   { buffer(11).asInstanceOf[Double] + groupAmount } else { buffer(11).asInstanceOf[Double] }
        buffer(12) =  if (metric("rappi"))  { buffer(12).asInstanceOf[Double] + groupAmount } else { buffer(12).asInstanceOf[Double] }
        buffer(13) =  if (metric("other"))  { buffer(13).asInstanceOf[Double] + groupAmount } else { buffer(13).asInstanceOf[Double] }
        buffer(14) =  if (metric("food"))   { buffer(14).asInstanceOf[Double] + lst_cod_bus.getOrElse("FOOD", 0.0) } else { buffer(14).asInstanceOf[Double] }
        buffer(15) =  if (metric("nfood"))  { buffer(15).asInstanceOf[Double] + lst_cod_bus.getOrElse("NFOOD", 0.0) } else { buffer(15).asInstanceOf[Double] }
    }
  }

  def merge(buffer1: MutableAggregationBuffer, buffer2: Row): Unit = {
    if (buffer1 != null && buffer2 != null) {
      for (i <- 0 until buffer1.size) buffer1(i) = buffer1(i).asInstanceOf[Double] + buffer2(i).asInstanceOf[Double]
    }
  }

  // Called after all the entries are exhausted.
  override def evaluate(buffer: Row): Any = {
    Seq(
        if (buffer(0).asInstanceOf[Double].isNaN)   {0.0} else { buffer(0).asInstanceOf[Double] }
        , if (buffer(1).asInstanceOf[Double].isNaN) {0.0} else { buffer(1).asInstanceOf[Double] }
        , if (buffer(2).asInstanceOf[Double].isNaN) {0.0} else { buffer(2).asInstanceOf[Double] }
        , if (buffer(3).asInstanceOf[Double].isNaN) {0.0} else { buffer(3).asInstanceOf[Double] }
        , if (buffer(4).asInstanceOf[Double].isNaN) {0.0} else { buffer(4).asInstanceOf[Double] }
        , if (buffer(5).asInstanceOf[Double].isNaN) {0.0} else { buffer(5).asInstanceOf[Double] }
        , if (buffer(6).asInstanceOf[Double].isNaN) {0.0} else { buffer(6).asInstanceOf[Double] }
        , if (buffer(7).asInstanceOf[Double].isNaN) {0.0} else { buffer(7).asInstanceOf[Double] }
        , if (buffer(8).asInstanceOf[Double].isNaN) {0.0} else { buffer(8).asInstanceOf[Double] }
        , if (buffer(9).asInstanceOf[Double].isNaN) {0.0} else { buffer(9).asInstanceOf[Double] }
        , if (buffer(10).asInstanceOf[Double].isNaN) {0.0} else { buffer(10).asInstanceOf[Double] }
        , if (buffer(11).asInstanceOf[Double].isNaN) {0.0} else { buffer(11).asInstanceOf[Double] }
        , if (buffer(12).asInstanceOf[Double].isNaN) {0.0} else { buffer(12).asInstanceOf[Double] }
        , if (buffer(13).asInstanceOf[Double].isNaN) {0.0} else { buffer(13).asInstanceOf[Double] }
        , if (buffer(14).asInstanceOf[Double].isNaN) {0.0} else { buffer(14).asInstanceOf[Double] }
        , if (buffer(15).asInstanceOf[Double].isNaN) {0.0} else { buffer(15).asInstanceOf[Double] }
    )
  }
}

