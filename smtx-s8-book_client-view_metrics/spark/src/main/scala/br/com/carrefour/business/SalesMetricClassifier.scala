package br.com.carrefour.business

import org.apache.spark.sql.Row
import org.apache.spark.sql.expressions.{MutableAggregationBuffer, UserDefinedAggregateFunction}
import org.apache.spark.sql.types._

class SalesMetricClassifier {
    def check(
        id_source_sale: Int
        , ds_site_format: String
        , cod_ind_site_ecomm: Int
        , cod_delivery: Int
        , cod_bus: Seq[String]
    ): Map[String, Boolean] = {
        val total = true
        val store = (id_source_sale == 1)
        val hyp   = (id_source_sale == 1) && (ds_site_format.contains("HIPERMERCADO")) 
        val sup   = (id_source_sale == 1) && (ds_site_format.contains("SUPERMERCADO"))
        val prx   = (id_source_sale == 1) && (ds_site_format.contains("EXPRESS"))
        val drg   = (id_source_sale == 1) && (ds_site_format.contains("DROGARIA"))
        val gas   = (id_source_sale == 1) && (ds_site_format.contains("POSTO"))
        val ecm   = (id_source_sale == 2)
        val std   = (id_source_sale == 2) && (cod_ind_site_ecomm == 1 && (cod_bus.contains("NFOOD") && Seq(0,1,2,3).contains(cod_delivery)) || (cod_bus.contains("FOOD") && cod_delivery == 3))
        val click = (id_source_sale == 2) && (cod_ind_site_ecomm == 1 && cod_bus.contains("NFOOD") && Seq(4,5).contains(cod_delivery))
        val drive = (id_source_sale == 2) && (cod_ind_site_ecomm == 1 && cod_bus.contains("FOOD") && cod_delivery == 4)
        val scan  = (id_source_sale == 2) && (cod_ind_site_ecomm == 0 && cod_delivery == 6)
        val rappi = (id_source_sale == 2) && (cod_ind_site_ecomm == 1 && cod_bus.contains("FOOD") && cod_delivery == 7)
        val other = (id_source_sale == 2) && ((cod_ind_site_ecomm == 0) || ((cod_bus.contains("FOOD") || !Seq(0,1,2,3,4,5,8).contains(cod_delivery)) && (cod_bus.contains("NFOOD") || !Seq(3,4,7).contains(cod_delivery)))) && ( cod_ind_site_ecomm == 1 || !Seq(6).contains(cod_delivery) )
        val food  = (cod_bus.contains("FOOD"))
        val nfood = (cod_bus.contains("NFOOD"))
       
        Map(
            "total"     -> total
            , "store"   -> store
            , "hyp"     -> hyp
            , "sup"     -> sup
            , "prx"     -> prx
            , "drg"     -> drg
            , "gas"     -> gas
            , "ecm"     -> ecm
            , "std"     -> std
            , "click"   -> click
            , "drive"   -> drive
            , "scan"    -> scan
            , "rappi"   -> rappi
            , "other"   -> other
            , "food"    -> food
            , "nfood"   -> nfood
        ) 
    }

}
