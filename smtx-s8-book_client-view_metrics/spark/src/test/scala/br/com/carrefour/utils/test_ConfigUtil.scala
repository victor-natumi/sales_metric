package br.com.carrefour.utils

// standard library imports
// related third party imports
import org.scalatest.FunSuite
import org.scalatest.prop.Checkers
// local application/library specific imports

class test_ConfigUtil extends FunSuite {
  test("[ConfigUtil] getMap metric.code_metric ") {
    val config = ConfigUtil.getMap("metric.code_metric")
    println(s"[*] ${config}")
  }
  test("[ConfigUtil] getSparkPartitions ") {
    val config = ConfigUtil.getSparkPartitions
    println(s"[*] ${config}")
  }
  test("[ConfigUtil] getHiveWarehouse ") {
    val config = ConfigUtil.getHiveWarehouse
    println(s"[*] ${config}")
  }
}
