package br.com.carrefour.business

// standard library imports
// related third party imports
/* ScalaCheck ScalaTest */
import org.scalatest.FunSuite
import org.scalatest.prop.Checkers
// local application/library specific imports

/**
  * Descrição da classe de teste
  *
  * @author Victor Yoshidi
  */

class test_getkpiGroup extends FunSuite with Checkers {

  test("[getkpiGroup] hipermercados") {
    val allKpi: List[String] = List("hipermercados","supermercados","express","drogaria","postos","e-commerce")
    val index = allKpi.indexOf("hipermercados")
    val format = allKpi(index).toUpperCase
    var lsKpi = List(format)
    for (y <- 0 until allKpi.size) {
      if (index != y) {
        if (y < index)
          lsKpi = lsKpi ++ List((allKpi(y)) + format)
        else
          lsKpi = lsKpi ++ List((format + allKpi(y)))
      }
    }
    lsKpi
    println(lsKpi)
  }
  test("[getkpiGroup] supermercados") {
    val allKpi: List[String] = List("hipermercados","supermercados","express","drogaria","postos","e-commerce")
    val index = allKpi.indexOf("supermercados")
    val format = allKpi(index).toUpperCase
    var lsKpi = List(format)
    for (y <- 0 until allKpi.size) {
      if (index != y) {
        if (y < index)
          lsKpi = lsKpi ++ List((allKpi(y)) + format)
        else
          lsKpi = lsKpi ++ List((format + allKpi(y)))
      }
    }
    lsKpi
    println(lsKpi)
  }
  test("[getkpiGroup] Combinantions supermercados") {
    val allKpi: Seq[String] = Seq("hipermercados","supermercados","express","drogaria","postos","e-commerce")
    val index = allKpi.indexOf("supermercados")
    val format = allKpi(index).toUpperCase
    val newAllKpi = allKpi.patch(index, Seq(format), 1)
    val combinationsList = newAllKpi.toSet.subsets.filter(_.contains(format)).map(m => m.toSeq.sortWith(_.toUpperCase < _.toUpperCase).mkString("|")).toList
    println(combinationsList)
    println(combinationsList.size)
  }
  test("[getkpiGroup] Combinantions hipermercados") {
    val allKpi: Seq[String] = Seq("hipermercados","supermercados","express","drogaria","postos","e-commerce")
    val index = allKpi.indexOf("hipermercados")
    val format = allKpi(index).toUpperCase
    val newAllKpi = allKpi.patch(index, Seq(format), 1)
    val combinationsList = newAllKpi.toSet.subsets.filter(_.contains(format)).map(m => m.toSeq.sortWith(_.toUpperCase < _.toUpperCase).mkString("|")).toList
    println(combinationsList)
    println(combinationsList.size)
  }
}

