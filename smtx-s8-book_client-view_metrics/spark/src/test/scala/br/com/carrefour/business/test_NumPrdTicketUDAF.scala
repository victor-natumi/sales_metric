package br.com.carrefour.business

// standard library imports
// related third party imports
/* Spark SQL */
import org.apache.spark.sql.Row
import org.apache.spark.sql.types._
/* ScalaCheck ScalaTest */
import org.scalatest.FunSuite
import org.scalatest.prop.Checkers
/* SparkTesting */
import com.holdenkarau.spark.testing._
// local application/library specific imports
import br.com.carrefour.model._

/**
* Descrição da classe de teste
*
* @author Victor Yoshidi
*/

class test_NumPrdTicketUDAF extends FunSuite with SharedSparkContext with DatasetSuiteBase with Checkers {
  object mock {
    val obj_Struct: StructType =
      StructType(
        StructField(SalesModel.Field.ID_SOURCE_SALE, IntegerType)
          :: StructField(SalesModel.Field.DS_SOURCE_SALE, StringType)
          :: StructField(SalesModel.Field.ID_SALE, IntegerType)
          :: StructField(SalesModel.Field.COD_SITE, StringType)
          :: StructField(SalesModel.Field.DS_SITE_FORMAT, StringType)
          :: StructField(SalesModel.Field.COD_UF, DecimalType(38, 10))
          :: StructField(SalesModel.Field.NUM_CONSUMER_DOCUMENT_PRIORITY, DecimalType(38, 10))
          :: StructField(SalesModel.Field.VAL_DOCUMENT_TOTAL_WITH_SERVICES, DecimalType(38, 10))
          :: StructField(SalesModel.Field.LST_SALES_ITEMS, ArrayType(UdafModel.ItemClientSchema))
          :: StructField(SalesModel.Field.COD_IND_SITE_ECOMM, IntegerType)
          :: StructField(SalesModel.Field.COD_BUS, StringType)
          :: StructField(SalesModel.Field.COD_DELIVERY, IntegerType)
          :: StructField(SalesModel.Field.QTY_TOTAL_COUPON_BURNT, DecimalType(38, 10))
          :: Nil
      )


    val obj_Data: Seq[Row] = Seq(
      Row(1, "", 1, "", "HYP", scala.math.BigDecimal("38"), scala.math.BigDecimal("38367738802"), scala.math.BigDecimal("38")
        , Seq(
          Row(
            scala.math.BigDecimal(12345)
            , scala.math.BigDecimal(1.00)
            , scala.math.BigDecimal(1.50)
            , scala.math.BigDecimal(1.50)
            , "FOOD"
            , ""
            , ""
            , ""
          )
          , Row(
            scala.math.BigDecimal(12345)
            , scala.math.BigDecimal(1.00)
            , scala.math.BigDecimal(1.50)
            , scala.math.BigDecimal(1.50)
            , "FOOD"
            , ""
            , ""
            , ""
          )
        )
        , 1, "", 1, scala.math.BigDecimal(13)
      )
    )
  }

    test("[NumPrdTicketUDAF] UDAF Metric Test") {
      val sqlContext = spark.sqlContext
      val sparkContext = spark.sparkContext

      val dfTest = sqlContext.createDataFrame(sparkContext.parallelize(mock.obj_Data), mock.obj_Struct)

      val businessInstance: SalesMetricBusiness = SalesMetricBusiness.getInstance("201906", "1")
      val result = businessInstance.getResult(businessInstance.applyMetric(dfTest, "7"))

      println("[*] Resultado: ")
      result.printSchema()
      result.show(false)
    }

}
