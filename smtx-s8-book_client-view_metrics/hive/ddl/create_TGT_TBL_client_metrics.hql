DROP TABLE IF EXISTS DB_DOLPHIN_TARGET.TBL_CLIENT_METRICS;
CREATE TABLE DB_DOLPHIN_TARGET.TBL_CLIENT_METRICS
(
    num_document string
    , cod_uf string
    , ds_grp_metric string
    , id_metric int
    , ds_metric string
    , val_metric decimal(38,10)
    , dth_ingestion timestamp
)
PARTITIONED BY (cod_year int, cod_month int, id_grp_metric int)
STORED AS ORC
TBLPROPERTIES (
    "orc.compress"="SNAPPY",
    "orc.compress.size"="262144"
);

