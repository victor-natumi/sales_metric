#!/bin/bash
################################################################################

## Nome: client_metrics.sh
## Description: Popula tabela de métricas de cliente DB_DOLPHIN_TARGET.TBL_CLIENT_METRICS

################################################################################

###  ..:: Fluxo do programa ::..

## 1 - Executa Spark Job para gerar métricas para o mês passado por parâmetro
##     a partir de dados da tabela TBL_SALES_ANALYTICS_STORE
##     e popula tabela TBL_CLIENT_METRICS

################################################################################

### ..:: Variaveis normais ::..

## Informacoes principais
JOB_NAME="client_metrics"
ENVIRONMENT="$1"
HOME_PATH_HDFS="/br/com/carrefour/dolphin/"
#HOME_PATH_GLUSTER="/gfsdata01/br/com/carrefour/${ENVIRONMENT}/dolphin/queries/"
HOME_PATH_GLUSTER="/home/brext2093/analytics_set/smtx-s6-book_cliente-metricas/"

## Dados do arquivo running
HDFS_CONF="${HOME_PATH_HDFS}landing/conf/"
NAME_CONF="${JOB_NAME}.out"

## Dados do arquivo de log
HOME_LOG="${HOME_PATH_GLUSTER}logs/"
MAX_SIZE="10"
FILE_LOG="${HOME_LOG}ingest_${JOB_NAME}.log"
FILE_LOG_OLD="${FILE_LOG}.tgz"

mkdir $HOME_LOG
touch $FILE_LOG

## Configuracoes spark
HDP_HOME=/usr/hdp/current
SPARK2_HOME=${HDP_HOME}/spark2-client
PHOENIX_HOME=${HDP_HOME}/phoenix-client
SQOOP_HOME=${HDP_HOME}/sqoop-client/lib
EXTRA_CLASS_PATH=${PHOENIX_HOME}/phoenix-spark2.jar:${PHOENIX_HOME}/phoenix-client.jar:${SQOOP_HOME}/ojdbc8.jar:/etc/hbase/conf
SPARK_MASTER=yarn
SPARK_MODE=client
KERBEROS_USER=sqoopjob@BR.WCORP.CARREFOUR.COM

## Localizacao dos scripts
JAR_NAME="sales_analitycs_job-assembly-0.1.jar"
JAR_PATH="${HOME_PATH_GLUSTER}jars"

################################################################################

### ..:: Variaveis das Chaves de Execucao ::..

DEBUG="y" #y or n
cd ${HOME_PATH_GLUSTER}
source ${HOME_PATH_GLUSTER}shell/common.sh

################################################################################
### ..:: Variaveis Auxiliares de Execucao ::..

LIST_METRIC=""
DT_FINAL=""
DT_EXECUTION=$2
EXECUTION_2=$3
EXECUTION_3=$4

################################################################################

### ..:: Funcoes ::..

function isrunning() {
    FILE_PATH="${HDFS_CONF}${NAME_CONF}"
    hadoop fs -test -f $FILE_PATH
    if [ $? == 0 ]
    then
            write_log "[WARN] - Processo de carga ${JOB_NAME} em andamento."
            exit 1
    else
            hadoop fs -touchz $FILE_PATH
            write_log "[INFO] - Arquivo ($NAME_CONF) criado."
            write_log "[INFO] - Processo de carga ${JOB_NAME} iniciado"
    fi
}

function delete_running() {
    FILE_PATH="${HDFS_CONF}${NAME_CONF}"
    hadoop fs -test -f $FILE_PATH
    if [ $? == 0 ]
    then
            hadoop fs -rm -f -r -skipTrash $FILE_PATH
            write_log "[INFO] - Arquivo ($NAME_CONF) deletado."
    else
            write_log "[ERRO] - Arquivo ($NAME_CONF) nao encontrado."
            put_elasticsearch_log "dolphin_ingest_log" "$JOB_NAME" "Erro" "0"
            exit 1
    fi
}

function spark_metric_calculation() {
  write_log "[INFO] - Executando spark submit para o job ${JOB_NAME}."
  ${SPARK2_HOME}/bin/spark-submit \
       --name ${JOB_NAME^^} \
       --master ${SPARK_MASTER} \
       --deploy-mode ${SPARK_MODE} \
       --driver-memory 6G \
       --executor-memory 6G \
       --conf spark.executor.extraClassPath=${EXTRA_CLASS_PATH} \
       --conf spark.driver.extraClassPath=${EXTRA_CLASS_PATH} \
       --conf spark.executor.cores=4 \
       --conf spark.driver.cores=4 \
       --conf spark.driver.memoryOverhead=4096 \
       --conf spark.executor.memoryOverhead=4096 \
       --conf spark.yarn.maxAppAttemps=4 \
       --conf spark.task.maxFailures=4 \
       --conf spark.speculation=true \
       --conf spark.dynamicAllocation.enabled=true \
       --conf spark.dynamicAllocation.initialExecutors=4 \
       --conf spark.dynamicAllocation.maxExecutors=8 \
       --conf spark.dynamicAllocation.minExecutors=4 \
       --conf spark.shuffle.service.enabled=true \
       --conf spark.shuffle.service.port=7337 \
       --keytab ${SQOOPJOB_KEYTAB} \
       --principal ${KERBEROS_USER} \
       --class br.com.carrefour.${JOB_NAME} \
       "${JAR_PATH}/${JAR_NAME}" "${DT_EXECUTION}" "${LIST_METRIC}"

  if [ $? == 0 ]
  then
    write_log "[INFO] - Arquivos criados com sucesso com sucesso."
  else
    write_log "[ERROR] - Falha ao realizar criacao dos arquivos."
    delete_running
    put_elasticsearch_log "dolphin_ingest_log" "$JOB_NAME" "Erro" "0"
    exit 1
  fi
}

function main() {
    write_log "-----------------------------------------------------------------"
    write_log "[INFO] - Inicio da carga ${JOB_NAME} do script ${JOB_NAME}.sh."

    if [ ! -z "$DT_EXECUTION" ] && [[ "$DT_EXECUTION" =~ ^(19|20)[0-9]{2}(0[1-9]|1[0-2])$ ]];then
        write_log "[INFO] - Data de processamento de ${JOB_NAME^^}: ${DT_EXECUTION}"
    else
        DT_EXECUTION=`date +"%Y%m" -d "-1 day"`
        write_log "[INFO] - Data de processamento de ${JOB_NAME^^} não fornecida! Usando data D-1: ${DT_EXECUTION}"
    fi

    if [ ! -z "$EXECUTION_2" ] && [[ "$EXECUTION_2" =~ ^(19|20)[0-9]{2}(0[1-9]|1[0-2])$ ]] && [ $EXECUTION_2 -gt $DT_EXECUTION ]
    then
        write_log "[INFO] - Data final de processamento fornecida! Processando ${JOB_NAME^^} de ${DT_EXECUTION} até ${EXECUTION_2}"
        DT_FINAL=${EXECUTION_2}
        if [ ! -z "$EXECUTION_3" ] && [[ "$EXECUTION_3" =~ ^[0-9]{,5}(,[0-9]{,5})*$ ]]
        then
            write_log "[INFO] - Lista de meticas fornecida! Executando para métricas $EXECUTION_3"
            LIST_METRIC=$EXECUTION_3
        else
            EXECUTION_3=""
        fi
    elif [ ! -z "$EXECUTION_2" ] && [[ "$EXECUTION_2" =~ ^[0-9]{,5}(,[0-9]{,5})*$ ]]
    then
        write_log "[INFO] - Lista de meticas fornecida! Executando para métricas $EXECUTION_2"
        LIST_METRIC=$EXECUTION_2
        DT_FINAL=${DT_EXECUTION}
        if [ ! -z "$EXECUTION_3" ]
        then
            write_log "[INFO] - Lista de meticas fornecida já fornecida! Ignorando ultimo argumento $EXECUTION_3"
        fi
        EXECUTION_3=""
    else
        EXECUTION_2=""
        EXECUTION_3=""
        DT_FINAL=${DT_EXECUTION}
    fi

    DATE_START=`date -u +%s`

    while [ $DT_EXECUTION -le $DT_FINAL ]
    do
        check_environment_variables
        run_start_kinit
        isrunning
        spark_metric_calculation 
        run_start_kinit
        delete_running
        var=$(date +%Y%m%d -d "${DT_EXECUTION}01 12:00 +1 month")
        DT_EXECUTION="${var:0:6}"
    done

    DATE_STOP=`date -u +%s`
    DATE_DURATION=`expr \( $DATE_STOP - $DATE_START \) / 60`

    put_elasticsearch_log "dolphin_ingest_log" "$JOB_NAME" "Success" "$DATE_DURATION"

    write_log "[INFO] - Fim da carga ${JOB_NAME} do script ${JOB_NAME}.sh."
    write_log "-----------------------------------------------------------------"
}

################################################################################

### ..:: Fluxo normal do programa ::..

[ "$DEBUG" == "n" ] && main &> /dev/null || main

exit 0

################################################################################

## ..:: Fim da execucao ::..       
